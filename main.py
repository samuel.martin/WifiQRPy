import logging

import matplotlib.pyplot as plt
from argparse import ArgumentParser

from modules.Camera import get_gray_scale_image
from modules.QR import read_qr
from modules.Wifi import connect_to_wifi


def main():
    parser = ArgumentParser()
    parser.add_argument(
        "-f",
        "--framerate",
        default="10",
        help="Framerate with which the camera is scanned, defaults to 10. Max might be limited by coputer performance.",
        type=int,
        required=False,
    )
    parser.add_argument(
        "-d", "--device", help="Camera device to use", default=-1, required=False
    )

    args = parser.parse_args()

    try:
        while True:
            image = get_gray_scale_image(args.device)
            ssid, password = read_qr(image)
            plt.imshow(image, cmap="gray")
            if ssid is not None:
                break
            plt.pause(1 / args.framerate)
        print(f"Found: SSID: '{ssid}' Password: '{password}'")
        if connect_to_wifi(ssid, password) != 0:
            logging.error(f"connection failed")
    except KeyboardInterrupt:
        print("Aborted")


if __name__ == "__main__":
    main()
