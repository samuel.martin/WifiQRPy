# WifiQRPy

Little project to scan the QR code to share a WIFI connection with the devices camera and automatically connect to it.
Uses OpenCV to read the camera, matplotlib to plot the resulting image and nmcli to connect to the wifi network.
