import logging

import cv2
import matplotlib.pyplot as plt


def get_gray_scale_image(device_index: int = -1) -> cv2.Mat:
    """
    Returns grayscale image captured from the devices camera
    :param device_index: Device index of the camera, defaults to -1
    """
    cap = cv2.VideoCapture(device_index)

    if not cap.isOpened():
        logging.error("Cannot open camera")
        exit()

    ret, frame = cap.read()

    # if frame is read correctly ret is True
    if not ret:
        logging.error("Can't receive frame (stream end?). Exiting ...")
        exit()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # When everything done, release the capture
    cap.release()
    return gray


if __name__ == "__main__":
    for i in range(30):
        image = get_gray_scale_image()
        plt.imshow(image, cmap="gray")
        plt.pause(0.1)

    plt.show()
