import logging
import re
from typing import Dict, Tuple, Optional

import cv2


def decode_wifi_text(text: str) -> Tuple[Optional[str], Optional[str]]:
    """
    Decode a wifi text from a qr-code in format WIFI:......
    :param text: input text
    :return: tuple(ssid, password) or tuple(None, None)
    """
    match = re.match(
        r"WIFI:([TSPH]):(.*);([TSPH]):(.*);([TSPH]):(.*);([TSPH]):(.*);.*", text
    )

    if match is None:
        return None, None
    t1, v1, t2, v2, t3, v3, t4, v4 = list(match.groups())
    res: Dict[str, str] = {
        t1: v1,
        t2: v2,
        t3: v3,
        t4: v4,
    }
    ssid, password = res["S"], res["P"]
    return ssid, password


def read_qr(image: cv2.Mat) -> Tuple[Optional[str], Optional[str]]:
    """
    read qr code from image and return wifi credentials
    :param image: input image in cv2 format
    :return: tuple(ssid, password) or tuple(None, None)
    """
    detector = cv2.QRCodeDetector()
    text, _, _ = detector.detectAndDecode(image)
    if text == "":
        text, _, _ = detector.detectAndDecodeCurved(image)

    if text == "":
        logging.error("unable to read qr code. Nothing found")
        return None, None

    #
    ssid, password = decode_wifi_text(text)
    if ssid is None:
        logging.error(f"unable to find wifi credentials in scanned text: {text}")

    return ssid, password


if __name__ == "__main__":
    image = cv2.imread("qr-test.png")
    print("image content:", read_qr(image))
