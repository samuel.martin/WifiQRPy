import os


def connect_to_wifi(ssid: str, password: str):
    """
    Connect to a given ssid and password with "nmcli"
    :param ssid: name of wifi network
    :param password: password of wifi network
    :return: exit code of bash command
    """
    cmd = f"nmcli d wifi connect '{ssid}' password '{password}'"
    return os.system(cmd)


if __name__ == "__main__":
    connect_to_wifi("test", "test")
